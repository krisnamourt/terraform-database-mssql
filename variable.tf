variable "aws_region" {
  description = "The AWS region to deploy into (e.g. us-east-2)."
  default     = "us-east-2"
}

# Resource

variable "replica_count" {
  description = "Number of cluster members."
}

variable "cpu_core_count" {
  description = "Number of cluster members."
}

variable "cpu_threads_per_core" {
  description = "Number of cluster members."
}


variable "instance_name" {
  description = "Instance name resources."
  type        = list(string)
}

variable "instance_type" {
  description = "Instance type."
  type        = list(string)
}

variable "private_ip" {
  description = "Private IP."
  type        = list(string)
}

variable "iam_instance_profile" {
  description = "Iam Instance Profile."
  type        = list(string)
}


variable "tag_name" {
  description = "Tag Name."
  type        = list(string)
}

variable "ebs_optimized" {
  description = "ebs_optimized."
  type        = list(string)
}

variable "availability_zone" {
  description = "availability_zone."
  type        = list(string)
}

variable "subnet_id" {
  description = "Subnet ID."
  type        = list(string)
}

variable "vpc_security_group_ids" {
  description = "The VPC secutiry group id."
  type        = list(string)
}

variable "monitoring" {
  description = "monitoring."
  type        = list(bool)
}



variable "key_name" {
  description = "Key Name."
}




# root_block_device

variable "volume_size" {
  description = "volume_size."
  type        = list(number)
}

variable "volume_id" {
  description = "EBS volume_id id."
  type        = list(string)
}


# ebs_block_device

/*variable "ebs_instance_id" {
 description = "EBS instance id."
   type    = list(string)
 } */

variable "ebs_snapshot_id" {
  description = "EBS snapshot id."
  type        = list(string)
}


variable "ebs_device_name" {
  description = "device_name."
  type        = list(string)
}

variable "ebs_block_iops" {
  description = "ebs_block_iops."
  type        = list(number)
}

variable "ebs_block_device_volume_size" {
  description = "ebs_block_device."
  type        = list(number)
}

variable "prod" {

}

#aws_route53_record

variable "dns_name" {

}

variable "dns_records" {
  type = list(string)
}


#aws_security_group

variable "security_group_name" {

}

variable "security_group_desc" {

}

variable "security_group_vpc_id" {

}

variable "security_tag_name" {

}

#aws_security_group_rule

variable "security_group_id" {

}

variable "security_group_sql_port" {
  type = number
}

variable "security_group_sql_prod" {
  description = "Indicates a different setup of rule."
}