replica_count          = 3
instance_name          = ["ami-6a5b710f", "ami-6a5b710f", "ami-05446e60"]
instance_type          = ["i3.2xlarge", "i3.2xlarge", "t2.micro"]
private_ip             = ["172.31.50.120", "172.31.64.91", "172.31.61.86"]
iam_instance_profile   = ["prod-75b86059-sqlserver", "", "ecsInstanceRole"]
tag_name               = ["prod-75b86059-sqlserver-01", "prod-75b86059-sqlserver-02", "prod-75b86059-sqlserver-quorum"]
ebs_optimized          = [true, false, true]
availability_zone      = ["us-east-2a", "us-east-2b", "us-east-2a"]
subnet_id              = ["subnet-a8704ac1", "subnet-0e423875", "subnet-a8704ac1"]
vpc_security_group_ids = ["sg-1209f979"]
monitoring             = [true, false, true]
cpu_core_count         = 4

cpu_threads_per_core = 2

key_name = "prod-75b86059-cluster-key"


volume_size = [100, 100, 30]
volume_id   = ["vol-0d503540efa88ceba", "vol-0e3b551020134a43e", "vol-043398ac03bdd8d37"]
#ebs_instance_id = ["i-01903500cfe0ba32b","i-01ba0e755bb7f6b1f","i-08e7d0f9fe17b0d29"]
ebs_snapshot_id              = ["vol-06034e4e2babd70c2", "vol-060c94e13049791a5", "vol-03adbcc0aef6ad502"]
ebs_device_name              = ["xvdf", "xvdf", "xvdb"]
ebs_block_device_volume_size = [1000, 1000, 8]
ebs_block_iops               = [3000, 3000, 100]

/*
terraform import aws_ebs_volume.ec2-ebs-sqlserver[2] vol-060c94e13049791a5
aws_ebs_volume.ec2-ebs-sqlserver[0]: Refreshing state... [id=vol-06034e4e2babd70c2]
aws_ebs_volume.ec2-ebs-sqlserver[2]: Refreshing state... [id=vol-060c94e13049791a5]
aws_ebs_volume.ec2-ebs-sqlserver[1]: Refreshing state... [id=vol-03adbcc0aef6ad502]

*/

#aws_route53_record

dns_name    = "beblue-cluster.database.beblue.com.br"
dns_records = []

#aws_security_group
prod                  = true
security_group_name   = "prod-75b86059-rds-sqlserver"
security_tag_name     = "prod-75b86059-rds-sqlserver"
security_group_desc   = "SQL Server access on local network"
security_group_vpc_id = "vpc-b67852df"
#aws_security_group_rule
security_group_id       = "sg-1209f979"
security_group_sql_port = 1450
security_group_sql_prod = false