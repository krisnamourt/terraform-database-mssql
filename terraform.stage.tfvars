replica_count          = 1
instance_name          = ["ami-016701e4375aed079"]
instance_type          = ["t2.large"]
private_ip             = ["172.31.57.166"]
iam_instance_profile   = ["stag-25589097-manager"]
tag_name               = ["staging-3a50077c-sqlserver"]
ebs_optimized          = [false]
availability_zone      = ["us-east-2a"]
subnet_id              = ["subnet-7290971b"]
vpc_security_group_ids = ["sg-ca292aa2"]
monitoring             = [false]
cpu_core_count         = 2

cpu_threads_per_core = 1

key_name = "staging-3a50077c-cluster-key"

volume_size                  = [100]
volume_id                    = []
ebs_instance_id              = []
ebs_snapshot_id              = ["snap-070b368b006ef1827"]
ebs_device_name              = ["xvdf"]
ebs_block_device_volume_size = [1024]
ebs_block_iops               = [3072]

#aws_route53_record

dns_name    = "beblue-stage.database.beblue.com.br"
dns_records = ["172.31.57.166"]

#aws_security_group
prod                  = false
security_group_name   = "staging-3a50077c-rds-sqlserver"
security_tag_name     = "staging-3a50077c-rds-sqlserver"
security_group_desc   = "Allow access to SQL Server"
security_group_vpc_id = "vpc-a586b1cc"
#aws_security_group_rule
security_group_id       = "sg-ca292aa2"
security_group_sql_port = 1433
security_group_sql_prod = false