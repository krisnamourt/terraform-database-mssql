
resource "aws_route53_record" "database_dns" {
  name    = "${var.dns_name}"
  records = var.dns_records

  ttl     = (var.prod == true ? 0 : 300)
  type    = "A"
  zone_id = "ZIKYJRTS2RU31"
  alias {
    evaluate_target_health = (var.prod == true ? false : null)
    name                   = (var.prod == true ? "prod-75b86059-sqlserver-cluster-0a382a7f7f54e4d5.elb.us-east-2.amazonaws.com" : "")
    zone_id                = (var.prod == true ? "ZLMOA37VPKANP" : "")
  }
}


resource "aws_instance" "ec2-sqlserver" {
  ami                         = var.instance_name[0]
  availability_zone           = var.availability_zone[0]
  ebs_optimized               = var.ebs_optimized[0]
  instance_type               = var.instance_type[0]
  monitoring                  = var.monitoring[0]
  key_name                    = "${var.key_name}"
  associate_public_ip_address = false
  cpu_core_count              = var.cpu_core_count
  cpu_threads_per_core        = var.cpu_threads_per_core
  get_password_data           = false
  ipv6_address_count          = 0
  ipv6_addresses              = []
  subnet_id                   = var.subnet_id[0]
  vpc_security_group_ids      = var.vpc_security_group_ids

  private_ip              = var.private_ip[0]
  source_dest_check       = true
  disable_api_termination = true
  iam_instance_profile    = var.iam_instance_profile[0]

  tags = {
    Name = var.tag_name[0]
  }
  tenancy     = "default"
  volume_tags = {}


  credit_specification {
    cpu_credits = (var.prod == true ? "" : "standard")
  }

  ebs_block_device {
    delete_on_termination = false
    device_name           = "${var.ebs_device_name[0]}"
    encrypted             = false
    iops                  = var.ebs_block_iops[0]
    snapshot_id           = (var.prod == true ? "" : "${var.ebs_snapshot_id[0]}")
    volume_size           = var.ebs_block_device_volume_size[0]
    volume_type           = "gp2"
  }

  root_block_device {
    delete_on_termination = true
    iops                  = var.ebs_block_iops[0]
    volume_size           = var.volume_size[0]
    volume_type           = "gp2"
  }

  timeouts {}

}


# resource "aws_ebs_volume" "ec2-sqlserver-ebs" {
#   count                       = (var.prod == true ? var.replica_count : 0 )
#   #count                       = (var.prod == true ? 1 : 0 )
#    #delete_on_termination = false
#     #device_name           = var.ebs_device_name[count.index]
#     availability_zone = "us-east-2a"
#     encrypted             = false    
#     snapshot_id           = var.ebs_snapshot_id[count.index]
#     #volume_size           = var.ebs_block_device_volume_size[count.index]
#     type           = "gp2"
# }


resource "aws_security_group" "vpc-security-group" {
  count       = (var.prod == true ? 1 : 0)
  description = "SQL Server access on local network"
  egress = [
    {
      cidr_blocks = [
        "0.0.0.0/0",
      ]
      description      = ""
      from_port        = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
      security_groups  = []
      self             = false
      to_port          = 0
    },
  ]
  ingress = [
    {
      cidr_blocks = [
        "172.31.0.0/16",
      ]
      description      = ""
      from_port        = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
      security_groups  = []
      self             = true
      to_port          = 0
    },
    {
      cidr_blocks      = []
      description      = ""
      from_port        = 1450
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "tcp"
      security_groups = [
        "sg-317fc859",
        "sg-bc0a3fd7",
      ]
      self    = false
      to_port = 1450
    },
  ]
  name = "prod-75b86059-rds-sqlserver"
  tags = {
    "Name" = "prod-75b86059-rds-sqlserver"
  }
  vpc_id = "vpc-b67852df"

  timeouts {}
}

#Only at PROD
resource "aws_instance" "ec2-sqlserver-replica" {
  count                       = (var.prod == true ? 1 : 0)
  ami                         = "ami-6a5b710f"
  associate_public_ip_address = false
  availability_zone           = "us-east-2b"
  cpu_core_count              = 4
  cpu_threads_per_core        = 2
  disable_api_termination     = true
  ebs_optimized               = true
  get_password_data           = false
  instance_type               = "i3.2xlarge"
  ipv6_address_count          = 0
  ipv6_addresses              = []
  key_name                    = "prod-75b86059-cluster-key"
  monitoring                  = true
  private_ip                  = "172.31.64.91"
  security_groups             = []
  source_dest_check           = true
  subnet_id                   = "subnet-0e423875"
  tags = {
    "Name" = "prod-75b86059-sqlserver-02"
  }
  tenancy     = "default"
  volume_tags = {}
  vpc_security_group_ids = [
    "sg-1209f979",
  ]

  ebs_block_device {
    delete_on_termination = false
    device_name           = "xvdf"
    encrypted             = false
    iops                  = 3000
    volume_size           = 1000
    volume_type           = "gp2"
  }

  root_block_device {
    delete_on_termination = true
    iops                  = 300
    volume_size           = 100
    volume_type           = "gp2"
  }

  timeouts {}
}

#Only at PROD
resource "aws_instance" "ec2-sqlserver-quorum" {
  count                       = (var.prod == true ? 1 : 0)
  ami                         = "ami-05446e60"
  associate_public_ip_address = false
  availability_zone           = "us-east-2a"
  cpu_core_count              = 1
  cpu_threads_per_core        = 1
  disable_api_termination     = true
  ebs_optimized               = false
  get_password_data           = false
  iam_instance_profile        = "ecsInstanceRole"
  instance_type               = "t2.micro"
  ipv6_address_count          = 0
  ipv6_addresses              = []
  key_name                    = "prod-75b86059-cluster-key"
  monitoring                  = false
  private_ip                  = "172.31.61.86"
  security_groups             = []
  source_dest_check           = true
  subnet_id                   = "subnet-a8704ac1"
  tags = {
    "Name" = "prod-75b86059-sqlserver-quorum"
  }
  tenancy     = "default"
  volume_tags = {}
  vpc_security_group_ids = [
    "sg-1209f979",
  ]

  credit_specification {
    cpu_credits = "standard"
  }

  ebs_block_device {
    delete_on_termination = false
    device_name           = "xvdb"
    encrypted             = false
    iops                  = 100
    volume_size           = 8
    volume_type           = "gp2"
  }

  root_block_device {
    delete_on_termination = true
    iops                  = 100
    volume_size           = 30
    volume_type           = "gp2"
  }

  timeouts {}
}


resource "aws_security_group" "vpc-security-group-dev-stage" {
  count       = (var.prod == true ? 0 : 1)
  name        = "${var.security_group_name}"
  description = "${var.security_group_desc}"
  vpc_id      = "${var.security_group_vpc_id}"

  tags = {
    Name = "${var.security_tag_name}"
  }
}



# resource "aws_security_group" "vpc-security-group" {
#   name        = "${var.security_group_name}"
#   description = "${var.security_group_desc}"
#   vpc_id      = "${var.security_group_vpc_id}"

#   tags = {
#     Name = "${var.security_tag_name}"
#   }
# }


resource "aws_security_group_rule" "vpc-security-group-dev-stage" {
  count             = (var.prod == true ? 0 : 1)
  cidr_blocks       = (var.security_group_sql_prod == true ? [] : ["172.31.0.0/16", ])
  description       = (var.security_group_sql_prod == true ? "" : "Allow SQL Server to vp...")
  from_port         = (var.security_group_sql_prod == true ? 0 : var.security_group_sql_port)
  ipv6_cidr_blocks  = []
  prefix_list_ids   = []
  protocol          = (var.security_group_sql_prod == true ? "-1" : "tcp")
  security_group_id = "${var.security_group_id}"
  self              = false
  to_port           = (var.security_group_sql_prod == true ? 0 : var.security_group_sql_port)
  type              = (var.security_group_sql_prod == true ? "egress" : "ingress")
}


resource "aws_security_group_rule" "vpc-security-group-dev-stage-1" {
  count             = (var.prod == true ? 0 : 1)
  cidr_blocks       = (var.security_group_sql_prod == true ? ["172.31.0.0/16"] : [])
  description       = (var.security_group_sql_prod == true ? "Allow postgres do local network" : "")
  from_port         = (var.security_group_sql_prod == true ? var.security_group_sql_port : 0)
  ipv6_cidr_blocks  = []
  prefix_list_ids   = []
  protocol          = (var.security_group_sql_prod == true ? "tcp" : "-1")
  security_group_id = "${var.security_group_id}"
  self              = true
  to_port           = (var.security_group_sql_prod == true ? var.security_group_sql_port : 0)
  type              = "ingress"
}

resource "aws_security_group_rule" "vpc-security-group-dev-stage-2" {
  count = (var.prod == true ? 0 : 1)
  cidr_blocks = [
    "172.31.0.0/16",
  ]
  from_port         = 3389
  ipv6_cidr_blocks  = []
  prefix_list_ids   = []
  protocol          = "tcp"
  security_group_id = "${var.security_group_id}"
  self              = false
  to_port           = 3389
  type              = "ingress"
}

resource "aws_security_group_rule" "vpc-security-group-dev-stage-3" {
  count = (var.prod == true ? 0 : 1)
  cidr_blocks = [
    "0.0.0.0/0",
  ]
  from_port         = 0
  ipv6_cidr_blocks  = []
  prefix_list_ids   = []
  protocol          = "-1"
  security_group_id = "${var.security_group_id}"
  self              = false
  to_port           = 0
  type              = "egress"
}


