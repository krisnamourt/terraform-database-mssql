replica_count = 1

instance_name          = ["ami-0090f8ef0d4f654e3"]
instance_type          = ["t2.large"]
private_ip             = ["172.31.54.215"]
iam_instance_profile   = ["dev-06bdcc01-manager"]
tag_name               = ["dev-06bdcc01-sqlserver"]
ebs_optimized          = [false]
availability_zone      = ["us-east-2a"]
subnet_id              = ["subnet-ddc22fb5"]
vpc_security_group_ids = ["sg-d6d5e3be"]
monitoring             = [false]
cpu_core_count         = 2

cpu_threads_per_core = 1


key_name = "dev-06bdcc01-cluster-key"

volume_size = [100]
volume_id   = []
//ebs_instance_id = ["vol-02d7a323736c3556d"]
ebs_snapshot_id              = ["snap-07462eb5d7f4cd148"]
ebs_device_name              = ["xvdf"]
ebs_block_device_volume_size = [1024]
ebs_block_iops               = [3072]

#aws_route53_record
dns_name    = "beblue-dev.database.beblue.com.br"
dns_records = ["172.31.54.215"]

#aws_security_group
prod                  = false
security_group_name   = "dev-06bdcc01-rds-sqlserver"
security_tag_name     = "dev-06bdcc01-rds-sqlserver"
security_group_desc   = "Allow SQL Server access"
security_group_vpc_id = "vpc-51f7e838"
#aws_security_group_rule
security_group_id       = "sg-d6d5e3be"
security_group_sql_port = 1433
security_group_sql_prod = false